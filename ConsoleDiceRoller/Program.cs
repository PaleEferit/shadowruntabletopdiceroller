﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDiceRoller
{
    class Program
    {
        private static Random rnd;
        private static List<int> hits;
        private static int sidesCount;
        public static void Main(string[] args)
        {
            /*initing*/
            rnd = new Random(DateTime.Now.Millisecond);
            sidesCount = 6;
            hits = new List<int>();
            hits.Add(5);
            hits.Add(6);
            /*interface running*/
            Console.WriteLine("\nHoi, chummer! Rolling dices program is active");
            bool analyze = ReadYN("\nNeed chance analysis?(y/n)\n Warning: analysis for dicepool size over 20 is not recomended. ");
            bool flag = true, rollFlag;
            List<double> chance;
            List<int> dices;

            int poolSize;
            do
            {
                poolSize = ReadInt("\nEnter the dicepool size please.");
                if (analyze)
                {
                    chance = AnalyzeChances(poolSize);
                    DisplayChances(chance);
                    rollFlag = ReadYN("\nRoll it?(y/n)");
                    if (rollFlag)
                    {
                        dices = Roll(poolSize);
                        DisplayHits(dices);
                    }
                    else
                    {
                        Console.WriteLine("\nDrek! What did I analyse it for?");
                    }
                }
                else 
                {
                    dices = Roll(poolSize);
                    DisplayHits(dices);
                }
                flag = ReadYN("\nDo we keep rolling?(y/n)");
                if (!flag)
                {
                    flag = !ReadYN("\nAre you sure?(y/n)");
                }
            } while (flag);
            Console.WriteLine("\nPress any key to close the program");
            Console.ReadKey();
        }

        private static void DisplayHits(List<int> dices)
        {
            Console.WriteLine(String.Format("\nRoll result for dicepull of {0}:\n", dices.Count));
            int hitsCount=0;
            for (int i = 0; i < dices.Count; i++)
            {
                if (IsHit(dices[i]))
                {
                    Console.Write(" [{0}]", dices[i]);
                    hitsCount++;
                }
                else
                {
                    Console.Write(" {0}", dices[i]);
                } 
            }
            Console.WriteLine("\nTotal hits: {0}", hitsCount);
        }

        private static void DisplayChances(List<double> chances)
        {
            Console.WriteLine("\nChances display for {0} size dicepool", chances.Count);
            for (int i = 0; i < chances.Count; i++)
            {
                Console.WriteLine("{0} hits - {1:0.000}% chance", i+1 , chances[i] * 100);
            }
        }

        private static bool IsHit(int number)
        {
            return hits.Contains(number);
        }

        private static List<double> AnalyzeChances(int dicePoolSize)
        {
            double hitChance = (double)(hits.Count) / sidesCount, unHitChance = 1-hitChance;
            List<double> chances = new List<double>();
            double tmpChance, hit, unhit;
            for (int i = 0; i < dicePoolSize; i++)
            {
                long multyplyer = Factorial(dicePoolSize) / ((Factorial(dicePoolSize - (i + 1)) * Factorial(i + 1)));
                hit = Math.Pow(hitChance, i + 1);
                unhit = Math.Pow(unHitChance, dicePoolSize - (i + 1));
                tmpChance = (hit*unhit)*multyplyer;
                chances.Add(tmpChance);
            }
            return chances;
        }

        private static long Factorial(int number)
        {
            long res = 1;
            if (number < 0)
            {
                return -1;
            }
            else 
            {
                if (number == 0)
                {
                    return 1;
                }
                else 
                {
                    for (int i = 1; i <= number; i++)
                    {
                        res *= i;
                    }
                }
            }
            return res;
        }

        private static List<int> Roll(int dicePoolSize)
        {
            List<int> dices = new List<int>();
            for (int i = 0; i < dicePoolSize; i++)
            {
                dices.Add(rnd.Next(6) + 1);
            }
            return dices;
        }

        private static bool ReadYN(string request)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine(request);
                char c = Console.ReadKey().KeyChar;
                c = Char.ToLower(c);
                if (c == 'y' || c == 'n')
                {
                    flag = false;
                    return c == 'y';
                }
                else 
                {
                    Console.WriteLine("\nIncorrect input. Try again please.");
                }
            }
            return false;
        }

        private static int ReadInt(string request)
        {
            int res=-1;
            bool flag = true;
            while (flag)
            {
                Console.WriteLine(request);
                try
                {
                    res = Int32.Parse(Console.ReadLine());
                    flag = false;
                }
                catch
                {
                    Console.WriteLine("\nParsing of number failed. Incorrect input. Try again please.");
                    flag = true;
                }
            }
            return res;
        }
    }
}
